﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;

namespace com112
{
    class CPU
    {
        public string name;
        public double speed;

        public CPU()
        {
            ManagementClass mc = new ManagementClass("Win32_Processor");
            foreach (ManagementObject mo in mc.GetInstances())
            {
                name = (string)mo["name"];
                speed = (UInt32)mo.Properties["CurrentClockSpeed"].Value * 0.001;
            }
        }
    }

    class MainBoard
    {
        public string manufacturer;
        public string name;

        public MainBoard()
        {
            ManagementClass mc = new ManagementClass("Win32_BaseBoard");
            foreach (ManagementObject mo in mc.GetInstances())
            {
                manufacturer = mo.GetPropertyValue("Manufacturer").ToString();
                name = mo.GetPropertyValue("Product").ToString();
            }
        }
    }

    class RAM
    {
        public string[] manufacturer = new string[8];
        public long[] size = new long[8];

        public long GetTotalSize()
        {
            long totalSize = 0;
            for (int i = 0; i < 8; i++)
                totalSize += size[i];
            return totalSize;
        }

        public int GetSizeGB(int i)
        {
            return BytesToGB(size[i]);
        }

        public int GetTotalSizeGB()
        {
            return BytesToGB(GetTotalSize());
        }

        public RAM()
        {
            ManagementClass mc = new ManagementClass("Win32_PhysicalMemory");
            int i = 0;
            foreach (ManagementObject mo in mc.GetInstances())
            {
                manufacturer[i] = Convert.ToString(mo["Manufacturer"]);
                size[i] = Convert.ToInt64(mo["Capacity"]);
                i++;
            }
        }

        public static int BytesToGB(long bytes)
        {
            long gb = bytes / 1024 / 1024 / 1024;
            return (int)gb;
        }
    }

    class VGA
    {
        public string name;
        public long vram;

        public int GetVRAMGB()
        {
            return RAM.BytesToGB(vram);
        }

        public VGA()
        {
            ManagementClass mc = new ManagementClass("Win32_VideoController");
            foreach (ManagementObject mo in mc.GetInstances())
            {
                name = Convert.ToString(mo["Name"]);
                vram = Convert.ToInt64(mo["AdapterRAM"]);
            }
        }
    }

    class Storage
    {
        public string[] name = new string[32];
        public long[] size = new long[32];

        public int GetSizeGB(int i)
        {
            return BytesToGB(size[i]);
        }

        public Storage()
        {
            ManagementClass mc = new ManagementClass("Win32_DiskDrive");
            foreach (ManagementObject mo in mc.GetInstances())
            {
                int i = Convert.ToInt32(mo["Index"]);
                name[i] = Convert.ToString(mo["Model"]);
                size[i] = Convert.ToInt64(mo["Size"]);
            }
        }

        public static int BytesToGB(long bytes)
        {
            long gb = bytes / 1000 / 1000 / 1000;
            return (int)gb;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            CPU cpu = new CPU();
            MainBoard mb = new MainBoard();
            RAM ram = new RAM();
            VGA vga = new VGA();
            Storage s = new Storage();
            Console.WriteLine("CPU: " + cpu.name + " @" + cpu.speed + "GHz");
            Console.WriteLine("MainBoard: " + mb.manufacturer + " " + mb.name);
            Console.WriteLine("RAM: " + ram.GetTotalSizeGB() + "GB");
            for (int i = 0; i < 8; i++)
            {
                if (ram.size[i] == 0)
                    continue;
                Console.WriteLine("\t" + ram.manufacturer[i] + " " + ram.GetSizeGB(i) + "GB");
            }
            Console.WriteLine("VGA: " + vga.name + " " + vga.GetVRAMGB() + "GB");
            for (int i = 0; i < 32; i++)
            {
                if (s.size[i] == 0)
                    continue;
                Console.WriteLine("Storage " + i + ": " + s.name[i] + " (" + s.GetSizeGB(i) + "GB)");
            }
        }
    }
}